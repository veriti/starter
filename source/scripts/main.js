(function($, w, console) {
  // Error-free console logs
  function log(message) {
    try { console.info(message); }
    catch (e) {}
    finally { return; }
  }

  // On document ready
  $(function() {
    log('starter script initiated!')
  });

})(jQuery, window, window.console);
